package net.csc.calculatrice2;

import java.util.*;
import java.util.ArrayList;
import java.util.Iterator;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.TilePane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;


public class App extends Application {
		
	private Button btn;
	String l;
	double r=0, t=0;
	String trucated;
	String x, y, op;
	public static boolean isequals;
	Label lbl = new Label ("");
	
    @Override
    public void start(Stage stage) {
    	
    	ArrayList<String> b1 = new ArrayList<String>();
    	b1.add("7");
    	b1.add("8");
    	b1.add("9");
    	b1.add("x");
    	b1.add("4");
    	b1.add("5");
    	b1.add("6");
    	b1.add("%");
    	b1.add("1");
    	b1.add("2");
    	b1.add("3");
    	b1.add("+");
    	b1.add("0");
    	b1.add(".");
    	b1.add("=");
    	b1.add("-");
    	Iterator<String> itr = b1.iterator();
    
    	BorderPane root = new BorderPane();
    	Scene scene = new Scene(root, 240, 290);
    	
    	// TOP
    	lbl.setFont(new Font (20));
    	lbl.setPadding(new Insets (10));
    	BorderPane.setAlignment(lbl, Pos.TOP_RIGHT);
    	root.setTop(lbl);
    	
    	// BOTTOM
    	TilePane tile = new TilePane();
    	for(String l : b1) {					
    		Button btn = new Button(l);
    		btn.setMinWidth(60);
    		btn.setMinHeight(60);
    		tile.getChildren().add(btn);
    		btn.setPrefSize(60, 60);
    		
    		btn.setOnAction(new HandleClick());		
    	}
    	root.setCenter(tile);
    	
    	stage.setTitle("Calculatrice");
    	stage.setScene(scene);
        stage.show();      
    }
    
class HandleClick implements EventHandler<ActionEvent> {
	
	public void handle (ActionEvent event) {			
		
		if (isequals == true) {
			lbl.setText(" ");
			isequals = false;
		}
		Button cliqued = (Button)event.getSource();    
		String l = cliqued.getText();   
		cliqued.setText(""+ l);	
		lbl.setText(""+ lbl.getText() + l);
		calcul(l);
		if(l.equals("=")) {
			lbl.setText(" " + trucated);
			x = null;
			y = null;
			op = null;
			r = 0;
			isequals = true;
		}
		
	}
}

    
public void calcul (String l) {
  
		boolean estChiffre = ((l.equals(".")) || (l.equals("0")) || (l.equals("1")) || (l.equals("2")) || (l.equals("3")) ||(l.equals("4")) ||(l.equals("5")) || (l.equals("6")) || (l.equals("7")) || (l.equals("8")) || (l.equals("9")));
		boolean estOperateur = ((l.equals("+")) || (l.equals("-")) || (l.equals("x")) || (l.equals("%")));
		
		
		if (x == null) 
		{
			if ((y == null) && (op == null) && (estChiffre == true)) 
			{
				x=l;
				System.out.println("x0 :" + x + "\t y0 : " + y);
			}
		}
		
		else
		{
			if (y == null) 
			{
					if ((estChiffre == true) && (op == null)) 
					{
						x+=l;
						System.out.println("x1 :" + x + "\t y1 : " + y);
					}
					if ((estOperateur == true) && (l.equals("+"))) 
					{
						op = "+";
						System.out.println("x2 :" + x + "\t y2 : " + y);
					}
					if ((estOperateur == true) && (l.equals("-")))
					{
						op = "-";
						System.out.println("x2 :" + x + "\t y2 : " + y);
					}
					if ((estOperateur == true) && (l.equals("x")))
					{
						op = "x";
						System.out.println("x2 :" + x + "\t y2 : " + y);
					}
					if ((estOperateur == true) && (l.equals("%")))
					{
						op = "%";
						System.out.println("x2 :" + x + "\t y2 : " + y);
					}
					if ((op != null) && (estChiffre == true)) 
					{
						y=l;
						System.out.println("x7 :" + x + "\t y7 : " + y);
					}
			}
			else
			{
				if ((op != null) && (estChiffre == true)) 
				{
					y+=l;
					System.out.println("x8 :" + x + "\t y8 : " + y);
				}
				else if ((op != null) && (estOperateur == true))
				{
					if (op.equals("+")) 
					{
						t = Double.valueOf(x) + Double.valueOf(y);
						x = String.valueOf(t);
						y = null;
						op = l;
						System.out.println("x10 :" + x + "\t y10 : " + y);
					}
					if (op.equals("-")) 
					{
						t = Double.valueOf(x) - Double.valueOf(y);
						x = String.valueOf(t);
						y = null;
						op = l;
						System.out.println("x11 :" + x + "\t y11 : " + y);
					}
					if (op.equals("%")) 
					{
						t = Double.valueOf(x) / Double.valueOf(y);
						x = String.valueOf(t);
						y = null;
						op = l;
						System.out.println("x12 :" + x + "\t y12 : " + y);
					}
					if (op.equals("x")) 
					{
						t = Double.valueOf(x) * Double.valueOf(y);
						x = String.valueOf(t);
						y = null;
						op = l;
						System.out.println("x13 :" + x + "\t y13 : " + y);
					}
					
				}
				else 
				{
					if ((l.equals("=")) && (op == "+")) 
					{
						Double.valueOf(y);
						r = Double.valueOf(x) + Double.valueOf(y);
						trucated = String.format("%.2f", r);
						System.out.println("x6 :" + x + "\t y6 : " + y + "\tr : " + r);
					}
					if ((l.equals("=")) && (op == "-")) 
					{
						Double.valueOf(y);
						r = Double.valueOf(x) - Double.valueOf(y);
						trucated = String.format("%.2f", r);
						System.out.println("x6 :" + x + "\t y6 : " + y + "\tr : " + r);
					}
					if ((l.equals("=")) && (op == "x")) 
					{
						Double.valueOf(y);
						r = Double.valueOf(x) * Double.valueOf(y);
						trucated = String.format("%.2f", r);
						System.out.println("x6 :" + x + "\t y6 : " + y + "\tr : " + r);
					}
					if ((l.equals("=")) && (op == "%")) 
					{
						Double.valueOf(y);
						r = Double.valueOf(x) / Double.valueOf(y);
						trucated = String.format("%.2f", r);
						System.out.println("x6 :" + x + "\t y6 : " + y + "\tr : " + r);
					}
				}
			}
			
			
		}
}

public static void main(String[] args) {
    launch();
}

}